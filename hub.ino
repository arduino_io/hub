#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>

#include "ccu.h"
#include "bool_component.h"
#include "percent_component.h"
#include "command_factory.h"
#include "Simple_Repository_IO.h"

#ifndef LED
#define LED D10
#endif

const String led = "led";
CentralCommandUnit ccu;

// change esp8266 board version to 2.5.2
// 115200 baud
const char* ssid = "ssid";
const char* password = "***";
ESP8266WebServer server(3030);

const String hostname = "Hub_A";

void connect() {
  WiFi.hostname(hostname);

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.print(".");
  }
  Serial.printf("Wifi connected\nIP address: %s\n", WiFi.localIP().toString().c_str());
}

void startUdpClient() {
  WiFiUDP udp;
  udp.begin(3031);
  IPAddress ip(224,0,0,1); // replace with the IP address of the destination
  udp.beginPacket(ip, 30000);
  udp.write(hostname.c_str());
  int status = udp.endPacket();
  Serial.printf("UDP: %s", status == 1 ? "success" : "fail");
}

void handleRoot() {
  Serial.printf("\nComm: %s\n", server.arg("comm").c_str());
  String result = ccu.execute(server.arg("comm"));
  Serial.printf("Result: %s\n", result.c_str());
  if (result != "ERROR") {
    server.send(200, "text/plain", result);
  } else {
    server.send(404, "text/plain", "Not found");
  }
}

void startTcpServer() {
  server.on("/", handleRoot);
  server.begin();
}

void startHub() {
  // Repository* hub = new Repository(hostname, 1);
  Repository* hub = new Repository(hostname, 2);
  BoolComponent* bc = new BoolComponent(LED_BUILTIN, led, Component::IoType::OUT);
  PercentComponent* pc = new PercentComponent(LED, "pwm_led", Component::IoType::OUT);
  hub->put(0, bc);
  hub->put(1, pc);
  ccu.setup(hub);
}

void setup() {
  Serial.begin(115200);
  startHub();

  connect();

  startUdpClient();
  startTcpServer();
}

void loop() {
  server.handleClient();
}