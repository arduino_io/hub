info:	      # This list
	@clear
	@echo ""
	@echo ''
	@echo ""
	@cat $(MAKEFILE_LIST) | grep -e "^[a-zA-Z_\-]*: *.*"

lint:         # Arduino Lint
	@arduino-lint --library-manager update

permission:
	@sudo chmod a+rw /dev/ttyUSB0
